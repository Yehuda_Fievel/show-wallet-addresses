import axios from 'axios';
import { useEffect, useState } from 'react';
import Circle from './Circle';

function App() {
  const [data, setData] = useState([]);
  const [clicked, setClicked] = useState<number | null>(null);

  useEffect(() => {
    axios.get('http://localhost:3000').then((res) => {
      setData(res.data);
    });
  }, []);

  return (
    <div className='wrapper'>
      {data.map((address, i) => (
        <Circle
          key={i}
          address={Object.keys(address)[0]}
          clicked={clicked === i}
          onClick={() => setClicked(i)}
        />
      ))}
    </div>
  );
}

export default App;
