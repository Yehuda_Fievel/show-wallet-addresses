type Props = {
  address: string;
  onClick: () => void;
  clicked: boolean;
};

function Circle(props: Props) {
  const clicked = props.clicked ? 'clicked' : '';
  return (
    <div className='circle-container' onClick={props.onClick}>
      <div className={`circle ${clicked}`}></div>
      {props.address}
    </div>
  );
}

export default Circle;
