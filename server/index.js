require("dotenv").config();
const express = require("express");
const cors = require('cors')
const { MongoClient } = require("mongodb");

const app = express();
const port = 3000;

const client = new MongoClient(process.env.MONG_URI);
let db = null;

app.use(cors());

app.get("/", async (req, res) => {
  const results = await db.collection('address').aggregate([{
    $group: {
      _id: '$address',
      severity: { $addToSet: '$severity' }
    }
  }]).toArray()
  const addressList = results.map(r => {
    return {
      [r._id]: {
        severity: getSeverity(r.severity),
        count: r.severity.length
      }
    }
  })
  res.send(addressList)
});

function getSeverity(severity) {
  if (severity.includes('high')) return 'high'
  if (severity.includes('medium')) return 'medium'
  if (severity.includes('low')) return 'low'
}

async function main() {
  await client.connect();
  db = client.db(process.env.DB_NAME);
}

main()
  .then(() => { console.log("Connected successfully to server"); })
  .catch((error) => { console.error(error); })

app.listen(port, () => {
  console.log(`Example app listening on port ${port}`);
});
